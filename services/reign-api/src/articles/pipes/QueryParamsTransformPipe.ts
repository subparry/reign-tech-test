import { PipeTransform, Injectable } from '@nestjs/common';
import { MonthWords } from '../interfaces/queryMetadata.interface';

@Injectable()
export class QueryParamsTransformPipe implements PipeTransform {
  transform(value: any) {
    value.page = parseInt(value.page) || 1;

    if (value.created_at_month) {
      value.created_at_month = MonthWords[value.created_at_month];
    }

    // Some clients do not wrap in array if there's no more than 1 element
    if (value._tags && typeof value._tags === 'string') {
      value._tags = [value._tags];
    }
    return value;
  }
}
