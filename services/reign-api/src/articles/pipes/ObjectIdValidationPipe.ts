import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { isValidObjectId } from 'mongoose';

@Injectable()
export class ObjectIdValidationPipe implements PipeTransform {
  transform(value: any) {
    if (isValidObjectId(value)) {
      return value;
    }
    throw new BadRequestException(
      `Validation failed: ${value} is not a valid ObjectId`,
    );
  }
}
