import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { getModelToken } from '@nestjs/mongoose';
import { Model, Query } from 'mongoose';
import {
  Article,
  ArticleDocument,
} from '../../src/articles/schemas/article.schema';
import { HttpModule } from '@nestjs/axios';
import { NotFoundException } from '@nestjs/common';

describe('ArticlesController', () => {
  let articlesController: ArticlesController;
  let articlesService: ArticlesService;
  let mockArticleModel: Model<ArticleDocument>;

  beforeEach(async () => {
    const articles: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [ArticlesController],
      providers: [
        ArticlesService,
        {
          provide: getModelToken(Article.name),
          useValue: Model,
        },
      ],
    }).compile();

    articlesController = articles.get<ArticlesController>(ArticlesController);
    articlesService = articles.get<ArticlesService>(ArticlesService);
    mockArticleModel = articles.get<Model<ArticleDocument>>(
      getModelToken(Article.name),
    );
  });

  describe('refetch', () => {
    it('should call model method "insertMany" with fetched data', () => {
      jest.spyOn(articlesService, 'fetchArticles').mockReturnValueOnce(
        new Observable((subscriber) => {
          subscriber.next({
            data: { hits: [{ created_at: '2019-06-06T12:00:00.000Z' }] },
          } as AxiosResponse);
        }),
      );
      jest
        .spyOn(mockArticleModel, 'insertMany')
        .mockImplementationOnce(() => [{}]);

      articlesController.refetch();

      expect(mockArticleModel.insertMany).toHaveBeenCalledWith(
        [{ created_at: '2019-06-06T12:00:00.000Z', created_at_month: 6 }],
        { ordered: false },
      );
    });
  });

  describe('index', () => {
    it('queries database for documents that matches filter criteria and their count', async () => {
      jest
        .spyOn(mockArticleModel, 'countDocuments')
        .mockImplementationOnce(
          () => Promise.resolve(5) as unknown as Query<any, any>,
        );
      const mockQuery = {
        limit: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        lean: jest.fn().mockResolvedValue(['article1', 'article2']),
      } as unknown as Query<any, any>;
      jest.spyOn(mockArticleModel, 'find').mockReturnValue(mockQuery);

      const response = await articlesController.index({
        page: 1,
        author: 'johannes',
        created_at_month: 10,
      });

      expect(mockArticleModel.countDocuments).toHaveBeenCalledWith({
        author: 'johannes',
        created_at_month: 10,
      });
      expect(response).toEqual({
        data: ['article1', 'article2'],
        meta: {
          page: 1,
          perPage: 5,
          totalItems: 5,
          totalPages: 1,
        },
      });
    });
  });

  describe('delete', () => {
    it('attempts to delete document with given id', async () => {
      jest
        .spyOn(mockArticleModel, 'deleteOne')
        .mockResolvedValue({ deletedCount: 1, acknowledged: true });

      const response = await articlesController.deleteArticle('1234');

      expect(mockArticleModel.deleteOne).toHaveBeenCalledWith({ _id: '1234' });
      expect(response).toBeUndefined();
    });

    it('throws NotFoundException if given id does not exist in DB', async () => {
      jest
        .spyOn(mockArticleModel, 'deleteOne')
        .mockResolvedValue({ deletedCount: 0, acknowledged: true });

      let thrownException: Error;
      try {
        await articlesController.deleteArticle('1234');
      } catch (error) {
        thrownException = error;
      }

      expect(thrownException).toBeInstanceOf(NotFoundException);
    });
  });
});
