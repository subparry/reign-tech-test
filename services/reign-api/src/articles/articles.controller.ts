import {
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Query,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { RESULTS_PER_PAGE } from '../../src/config/constants';
import { IndexResponse } from '../../src/interfaces/indexResponse.interface';
import { ArticlesService } from './articles.service';
import { IndexQueryParams } from './interfaces/queryMetadata.interface';
import { QueryParamsTransformPipe } from './pipes/QueryParamsTransformPipe';
import { SchemaValidationPipe } from './pipes/SchemaValidationPipe';
import { Article } from './schemas/article.schema';
import { searchParamsSchema } from './schemas/searchParamsSchema';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ObjectIdValidationPipe } from './pipes/ObjectIdValidationPipe';
import { JwtAuthGuard } from '../../src/auth/guards/jwt-auth.guard';

@ApiTags('Articles')
@ApiBearerAuth()
@Controller('articles')
@UseGuards(JwtAuthGuard)
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @ApiOperation({
    description:
      'Fetches latest articles from HN on demand and populates DB with whatever new ones are there',
  })
  @ApiResponse({
    status: 202,
    description:
      'The server acknowledged the intention and started processing the request. The outcome is uncertain and the only way to know if it succeeded would be to check if there are more articles in the database or inspecting the server logs',
  })
  @Post('refetch')
  @HttpCode(202)
  refetch() {
    this.articlesService.saveNewData();
  }

  @ApiResponse({
    status: 200,
    description:
      'Returns list of articles that match the given search params and pagination metadata',
  })
  @Get()
  @UsePipes(
    QueryParamsTransformPipe,
    new SchemaValidationPipe(searchParamsSchema),
  )
  async index(
    @Query() query: IndexQueryParams,
  ): Promise<IndexResponse<Article>> {
    const { page, ...queryFilters } = query;

    const totalItems = await this.articlesService.countAllBy(queryFilters);
    const articles = await this.articlesService.getPaginatedBy(
      queryFilters,
      page,
    );

    return {
      meta: {
        page,
        perPage: RESULTS_PER_PAGE,
        totalItems,
        totalPages: Math.ceil(totalItems / RESULTS_PER_PAGE),
      },
      data: articles,
    };
  }

  @UsePipes(ObjectIdValidationPipe)
  @HttpCode(204)
  @Delete(':id')
  async deleteArticle(@Param('id') id: string) {
    const deleted = await this.articlesService.deleteById(id);
    if (!deleted.deletedCount) {
      throw new NotFoundException();
    }
  }
}
