import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { AxiosResponse } from 'axios';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { RESULTS_PER_PAGE } from '../../src/config/constants';
import { QueryFilters } from './interfaces/queryMetadata.interface';
import { Article, ArticleDocument } from './schemas/article.schema';

@Injectable()
export class ArticlesService {
  constructor(
    private httpService: HttpService,
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
  ) {}

  fetchArticles(): Observable<AxiosResponse<any>> {
    return this.httpService.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
  }

  @Cron(CronExpression.EVERY_HOUR)
  saveNewData() {
    const observable = this.fetchArticles();

    observable.subscribe({
      next: async (res) => {
        const fetchedArticles: Array<Article> = res.data.hits.map(
          (article: Article) => ({
            ...article,
            created_at_month: new Date(article.created_at).getMonth() + 1,
          }),
        );
        try {
          const result = await this.articleModel.insertMany(fetchedArticles, {
            ordered: false,
          });
          console.info(`Insert successful: ${result.length} articles inserted`);
        } catch (error) {
          console.info(
            `Insert operation finished. Some records could not be inserted because they were already present in the DB.`,
          );
          console.info(
            `Inserted ${error.result.result.nInserted} articles out of ${res.data.hits.length}`,
          );
        }
      },
      error: console.error,
    });
  }

  processQueryFilters(filters: QueryFilters) {
    if (filters._tags) {
      return { ...filters, _tags: { $all: filters._tags } };
    }
    return filters;
  }

  countAllBy(filters: QueryFilters) {
    return this.articleModel.countDocuments(this.processQueryFilters(filters));
  }

  getPaginatedBy(filters: QueryFilters, page: number) {
    return this.articleModel
      .find(this.processQueryFilters(filters))
      .limit(RESULTS_PER_PAGE)
      .skip((page - 1) * RESULTS_PER_PAGE)
      .lean();
  }

  deleteById(id: string) {
    return this.articleModel.deleteOne({ _id: id });
  }
}
