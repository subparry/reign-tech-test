import { ObjectId } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class ArticleParams {
  @ApiProperty()
  id: ObjectId;
}
