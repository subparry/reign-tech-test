import { ApiPropertyOptional } from '@nestjs/swagger';

export enum MonthWords {
  january = 1,
  february,
  march,
  april,
  may,
  june,
  july,
  august,
  september,
  october,
  november,
  december,
}

export class QueryFilters {
  @ApiPropertyOptional()
  author?: string;
  @ApiPropertyOptional({ type: [String] })
  _tags?: string[];
  @ApiPropertyOptional()
  story_title?: string;
  @ApiPropertyOptional({ enum: MonthWords })
  created_at_month?: MonthWords;
}

export class IndexQueryParams extends QueryFilters {
  @ApiPropertyOptional({ default: 1 })
  page: number;
}
