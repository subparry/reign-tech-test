import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongoSchema } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  created_at: Date;

  @Prop()
  created_at_month: number;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: number;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop()
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop([String])
  _tags: string[];

  @Prop({ unique: true })
  objectID: string;

  @Prop({ type: MongoSchema.Types.Mixed })
  _highlightResult: Record<string, unknown>;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
