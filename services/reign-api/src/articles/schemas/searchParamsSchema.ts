import * as Joi from 'joi';

export const searchParamsSchema = Joi.object({
  page: Joi.number().min(1),
  author: Joi.string(),
  story_title: Joi.string(),
  created_at_month: Joi.number().min(1).max(12),
  _tags: Joi.array().items(Joi.string()),
});
