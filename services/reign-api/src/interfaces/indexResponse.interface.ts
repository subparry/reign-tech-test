export class QueryMetadata {
  page: number;
  perPage: number;
  totalItems: number;
  totalPages: number;
}

export interface IndexResponse<T> {
  data: Array<T>;
  meta: QueryMetadata;
}
