import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { UsersService } from '../../src/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string) {
    const user = await this.usersService.findByEmail(email);
    if (user) {
      const valid = await bcrypt.compare(password, user.passwordHash);
      return valid && user;
    }
  }

  async login(user: { email: string }) {
    const payload = { email: user.email };
    return { access_token: this.jwtService.sign(payload) };
  }
}
