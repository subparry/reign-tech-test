import * as Joi from 'joi';

export const createUserBodySchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(6).required(),
});
