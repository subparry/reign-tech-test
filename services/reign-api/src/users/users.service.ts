import * as bcrypt from 'bcrypt';
import { Model, Query } from 'mongoose';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { CreateUserDto } from './dto/create-user.dto';
import { UserDocument, User } from './schemas/user.schema';
import { SALT_ROUNDS } from '../../src/config/constants';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createUserDto: CreateUserDto) {
    try {
      const user = await this.userModel.create({
        email: createUserDto.email,
        passwordHash: await bcrypt.hash(createUserDto.password, SALT_ROUNDS),
      });
      return user;
    } catch (error) {
      throw new BadRequestException(
        `There was an error trying to create the user: ${error.message}`,
      );
    }
  }

  findByEmail(email: string): Query<User, UserDocument> {
    return this.userModel.findOne({ email });
  }
}
