import { Body, Controller, Post, UsePipes } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SchemaValidationPipe } from '../../src/articles/pipes/SchemaValidationPipe';

import { CreateUserDto } from './dto/create-user.dto';
import { createUserBodySchema } from './schemas/createUserBody.schema';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @UsePipes(new SchemaValidationPipe(createUserBodySchema))
  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    return await this.usersService.create(createUserDto);
  }
}
