export const RESULTS_PER_PAGE = 5;
export const SALT_ROUNDS = 10;

// JWT secret exposed unsafely for sake of simplicity of demo app. Never do this in production!
export const JWT_SECRET = 'please_move_me_to_a_safer_place';
