# Reign tech test

Technical test for Reign back-end position

## Description

This is a simple API that fetches data from HackerNews every hour and stores the latest articles in a mongoDB collection for later paginated and filtered retrieval. Every endpoint of the articles resource is protected by a Bearer Authorization auth schema.

## Getting started

Provided you have docker & docker-compose on your system, from the project root run:

```bash
docker-compose -f docker-compose.dev.yml up
```

This will run the services on development mode with volumes mapped to your filesystem so that you can continue developing the project on your local machine while enjoying the benefits of having a containerized app.

The backend runs on `http://localhost:3000` while the client runs in `http://localhost:3001`

**NOTE: The app is not production ready because the client needs a web server like nginx to be able to be served properly. Anyway, there is a docker-compose.prod.yml with the initial steps done**

## Yarn packages

The project uses **Yarn** as package manager and it is set up as a monorepo using yarn workspaces.

This means that in order to modify dependencies in the client or the backend, you need to issue the commands from the root like this:

```bash
yarn workspace reign-api add luxon
```

or

```bash
yarn workspace client add styled-components
```

## API docs and Bearer Token

The project includes a bare-bones UI in the form of a react app to interact with the API, but there is also the option of interacting directly via the Swagger UI. In order to do this, first run the services with `docker-compose` and then visit `http://localhost:3000/api/docs`.

You will have to create an user first using the `POST /users` endpoint. There is a sample payload already provided for ease of use.

After that, you will need to log in using `POST /auth/login` and the credentials you specified in the previous step (if you used the defaults, this is already pre filled for you). This endpoint will respond by giving you an `access_token`.
Click on the **Authorize** button on the top right corner of the Swagger UI and paste the `access_token` in the `value` field.

Now you will be able to perform authenticated requests to all `/articles` endpoints.

## Initial articles data

Since the API updates its own database every hour, it is possible that the first time you run it, you will not have any data in the DB. You can force an articles fetch and DB write using the `POST articles/refetch` endpoint. In order to do this you will need to log in first by following the above instructions.

## Room for Improvement

There is always lots of room for improvement in any project, but in this one I should mention that filters should work with partial matches (fuzzy search), the client app is super rough because it wasn't the center of this exercise, I didn't bother in making the whole thing production ready, and the test coverage should be way upper than 30% but I hope that I was able to convey to some extent the reach of my abilities.
