import { serializeQuery } from '../utils/serializeQuery';

class Api {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  makeRequest({ method, endpoint, payload, token }) {
    const initOptions = {
      method,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    if (payload) {
      initOptions.body = JSON.stringify(payload);
    }
    if (token) {
      initOptions.headers.Authorization = `Bearer ${token}`;
    }
    return fetch(`${this.baseUrl}${endpoint}`, initOptions).then((res) => {
      if (res.status !== 202 && res.status !== 204) {
        return res.json();
      }
      return Promise.resolve({});
    });
  }

  login({ email, password }) {
    return this.makeRequest({
      method: 'POST',
      endpoint: '/auth/login',
      payload: { email, password },
    });
  }

  signUp({ email, password }) {
    return this.makeRequest({
      method: 'POST',
      endpoint: '/users',
      payload: { email, password },
    });
  }

  getArticles({ filters, token }) {
    const query = serializeQuery(filters);
    const endpoint = `/articles?${query}`;

    return this.makeRequest({
      method: 'GET',
      endpoint,
      token,
    });
  }

  refetch({ token }) {
    return this.makeRequest({
      method: 'POST',
      endpoint: '/articles/refetch',
      token,
    });
  }

  removeArticle({ token, articleId }) {
    return this.makeRequest({
      method: 'DELETE',
      endpoint: `/articles/${articleId}`,
      token,
    });
  }
}

export default new Api('http://localhost:3000');
