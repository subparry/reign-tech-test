import styled from 'styled-components';
import { Button } from '../Shared/Button';
import { PageContainer } from '../Shared/Containers';
import Form from './Form';

const Container = styled(PageContainer)`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
`;

const UserSession = ({ token, handleLogin, handleLogout }) => {
  return (
    <Container>
      {token ? (
        <Button onClick={handleLogout}>Log out</Button>
      ) : (
        <>
          <Form
            submitMsg="Sign Up for a new account"
            successMsg="Account created!"
            action="signUp"
          />
          <Form
            submitMsg="Log in with existing account"
            action="login"
            afterSuccess={(data) => handleLogin(data)}
          />
        </>
      )}
    </Container>
  );
};

export default UserSession;
