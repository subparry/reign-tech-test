import { useState } from 'react';
import styled from 'styled-components';
import api from '../../services/api';
import { Button } from '../Shared/Button';
import { ComponentContainer } from '../Shared/Containers';

const Container = styled(ComponentContainer)`
  transition: background-color 0.3s;
  display: flex;
  max-width: 500px;
  background-color: ${({ stateColor }) => stateColor};
  .inputs {
    display: flex;
    flex-direction: column;
    justify-content: center;
    label {
      margin: 10px;
      display: flex;
      justify-content: space-between;
      input {
        margin-left: 15px;
      }
    }
  }
`;

const Submit = styled(Button)`
  font-weight: bold;
  min-width: 200px;
`;

const submitStates = {
  SUCCESS: 'rgba(0,200,0,0.5)',
  FAILED: 'rgba(200,0,0,0.5)',
  IDLE: 'white',
};

const Form = ({ action, successMsg, afterSuccess = () => {}, submitMsg }) => {
  const [email, setEmail] = useState('');
  const [pass, setPass] = useState('');
  const [submitState, setSubmitState] = useState(submitStates.IDLE);
  const [msg, setMsg] = useState(null);

  const submitHandler = (e) => {
    e.preventDefault();
    if (!email || !pass) return;

    api[action]({ email, password: pass })
      .then((json) => {
        setSubmitState(submitStates.SUCCESS);
        setEmail('');
        setPass('');
        setMsg(successMsg);
        afterSuccess(json);
      })
      .catch((err) => {
        setSubmitState(submitStates.FAILED);
        setMsg(err.message);
      })
      .finally(() => {
        setTimeout(() => {
          setSubmitState(submitStates.IDLE);
          setMsg(null);
        }, 1000);
      });
  };
  return (
    <Container as="form" stateColor={submitState} onSubmit={submitHandler}>
      <div className="inputs">
        <label>
          Email
          <input
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            placeholder="Email"
            type="email"
            name="email"
          />
        </label>
        <label>
          Password
          <input
            required
            type="password"
            name="password"
            onChange={(e) => setPass(e.target.value)}
            value={pass}
            placeholder="Password"
          />
        </label>
      </div>
      <Submit disabled={msg}>{msg || submitMsg}</Submit>
    </Container>
  );
};

export default Form;
