import { useState } from 'react';
import Articles from '../Articles';
import UserSession from '../UserSession';

const App = () => {
  const [token, setToken] = useState(localStorage.getItem('token'));

  const handleLogin = ({ access_token }) => {
    setToken(access_token);
    localStorage.setItem('token', access_token);
  };

  const handleLogout = () => {
    setToken(null);
    localStorage.removeItem('token');
  };

  return (
    <>
      <UserSession
        token={token}
        handleLogin={handleLogin}
        handleLogout={handleLogout}
      />
      {token ? (
        <Articles token={token} />
      ) : (
        <h3>You must be logged in to interact with the articles</h3>
      )}
    </>
  );
};

export default App;
