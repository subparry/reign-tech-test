import styled from 'styled-components';
import { Button } from '../Shared/Button';
import { ComponentContainer } from '../Shared/Containers';

const Container = styled(ComponentContainer)`
  display: flex;
  align-items: center;
  flex-direction: column;

  .inputs {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
  }

  .centered {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .search {
    max-width: 150px;
  }
`;

const filterInputsData = [
  { name: 'author', placeholder: 'Author' },
  { name: 'story_title', placeholder: 'Story title' },
  { name: 'created_at_month', placeholder: 'Month of creation (e.g. july)' },
  { name: '_tags', placeholder: 'Tags separated by space' },
];

const FiltersAndPagination = ({
  setField,
  filters,
  pagination,
  fetchArticles,
  refetch,
}) => {
  const makeFieldSetter = (field) => (e) => setField(field, e.target.value);
  return (
    <Container>
      <h2>Filters and pagination</h2>
      <div className="inputs">
        <Button
          onClick={() => setField('page', Math.max(filters.page - 1, 1))}
          disabled={filters.page === 1}
        >
          Prev Page
        </Button>
        {filterInputsData.map(({ name, placeholder }) => (
          <input
            type="text"
            name={name}
            onChange={makeFieldSetter(name)}
            placeholder={placeholder}
            key={name}
            value={filters[name]}
          />
        ))}
        <Button
          onClick={() =>
            setField('page', Math.min(filters.page + 1, pagination.totalPages))
          }
          disabled={filters.page === pagination.totalPages}
        >
          Next Page
        </Button>
      </div>
      <Button className="search" onClick={fetchArticles}>
        Search
      </Button>
      <div className="centered">
        <p>Current Page: {filters.page}</p>
        <p>Total pages: {pagination.totalPages}</p>
        <p>Total results: {pagination.totalItems}</p>
        <Button onClick={refetch}>Refetch articles now</Button>
        <p>
          <em>
            <small>
              Give it some time to fetch articles from HN and then refresh this
              window to see if there are new articles available
            </small>
          </em>
        </p>
      </div>
    </Container>
  );
};

export default FiltersAndPagination;
