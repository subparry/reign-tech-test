import { Button } from '../Shared/Button';

const buildList = (articles) =>
  articles.map((article) => {
    return (
      <div key={article._id}>
        <h2>
          <a href={article.story_url} target="_blank" rel="noreferrer">
            {article.story_title}
          </a>
        </h2>
        <p>
          <small>{article.created_at}</small>
        </p>
        <p>{article.comment_text}</p>
        <h4>Tags: {article._tags.join(' | ')}</h4>
        <Button data-article-id={article._id}>Delete article</Button>
        <hr />
      </div>
    );
  });

const ArticleList = ({ articles, removeArticleListener }) => {
  return <div onClick={removeArticleListener}>{buildList(articles)}</div>;
};

export default ArticleList;
