import { useEffect, useState } from 'react';
import api from '../../services/api';
import ArticleList from './ArticleList';
import FiltersAndPagination from './FiltersAndPagination';

const transformations = { _tags: (v) => v.split(' ').map((tag) => tag.trim()) };

const identityFunction = (v) => v;

const transformFilters = (filters) => {
  const transformedFilters = {};
  Object.keys(filters).forEach((key) => {
    if (filters[key]) {
      const transformFunction = transformations[key] || identityFunction;
      transformedFilters[key] = transformFunction(filters[key]);
    }
  });
  return transformedFilters;
};

const Articles = ({ token }) => {
  const [filters, setFilters] = useState({
    page: 1,
    author: '',
    story_title: '',
    _tags: '',
  });

  const [articles, setArticles] = useState(null);
  const [pagination, setPagination] = useState(null);

  const setField = (field, value) =>
    setFilters((prev) => ({ ...prev, [field]: value }));

  const fetchArticles = () => {
    setPagination(null);
    setArticles(null);
    api
      .getArticles({ filters: transformFilters(filters), token })
      .then((json) => {
        setArticles(json.data);
        setPagination(json.meta);
      });
  };

  const refetch = () => api.refetch({ token }).then(() => fetchArticles());

  const removeArticleListener = (e) => {
    const { articleId } = e.target.dataset;
    if (articleId) {
      api.removeArticle({ token, articleId }).then(() => {
        setArticles((prev) => prev.filter(({ _id }) => _id !== articleId));
      });
    }
  };

  useEffect(fetchArticles, [filters.page]); // eslint-disable-line
  return (
    <>
      {pagination && (
        <FiltersAndPagination
          setField={setField}
          filters={filters}
          pagination={pagination}
          fetchArticles={fetchArticles}
          refetch={refetch}
        />
      )}
      {articles && (
        <ArticleList
          articles={articles}
          removeArticleListener={removeArticleListener}
        />
      )}
    </>
  );
};

export default Articles;
