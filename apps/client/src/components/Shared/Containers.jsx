import styled from 'styled-components';

export const PageContainer = styled.div`
  width: 100%;
`;

export const ComponentContainer = styled.div`
  border: 5px solid darkgray;
  border-radius: 25px;
  padding: 5px 12px;
`;
