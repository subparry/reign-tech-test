import styled from 'styled-components';

export const Button = styled.button`
  background-color: white;
  color: black;
  border: 4px solid black;
  border-radius: 4px;
  cursor: pointer;
  outline: none;

  transition: color 0.3s, background-color 0.3s, border-color 0.6s;
  &:hover {
    background-color: black;
    color: white;
  }

  &:focus {
    border-color: #ffee00;
    background-color: black;
    color: #ffee00;
  }

  &:disabled {
    background-color: darkgray;
    border-color: lightgray;
    pointer-events: none;
  }
`;
